# Gephi in docker

A Dockerfile to user gephi.

## Usage

```sh
docker run --rm -it -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v $HOME/workspace:/home/developer/workspace graham2071/gephi
```

Change `$HOME/workspace` to whatever you want. The folder shall exist with write permission. Eg: `mkdir -m 664 -p $HOME/workspace && chmod u+s $HOME/workspace`.

## Build

```sh
docker build . --tag=graham2071/gephi
```

Or with s specific release:

```sh
docker build --build-arg GEPHI_VERSION=0.9.2 . --tag=graham2071/gephi
```
