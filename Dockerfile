FROM openkbs/netbeans

ARG GEPHI_VERSION=0.9.2
RUN wget https://github.com/gephi/gephi/releases/download/v${GEPHI_VERSION}/gephi-${GEPHI_VERSION}-linux.tar.gz &&\
        tar xzvf gephi-$GEPHI_VERSION-linux.tar.gz &&\
        rm gephi-$GEPHI_VERSION-linux.tar.gz &&\
        ln -s gephi-${GEPHI_VERSION} gephi

CMD ./gephi/bin/gephi
